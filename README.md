# Gromacs-SWAXS package recipes for Spack

```
$ git clone https://gitlab.com/cbjh/packaging/spack-recipes.git ~/.spack-cbjh-packages
$ spack repo add ~/.spack-cbjh-packages
$ spack install gromacs-swaxs+cuda
```
